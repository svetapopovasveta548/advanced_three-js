// деструктуризация позволяет разбивать элементы массива или объекта на переменные, что позволяет обращаться напрямую к ним


//1 
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет", "Пирс"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clients3 = new Set([...clients1, ...clients2]);
console.log(clients3);



//2 
const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17, 
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];


const characters2 = characters.map((characters) => {
const {name, lastName, age} = characters
return {
  name: name,
  lastName: lastName,
  age: age,
};
})

console.log(characters2);

//3
const user1 = {
  name: "John",
  years: 30
};

const { name: imya, years: vik, isAdmin=false} = user1;

console.log(imya);
console.log(vik);
console.log(isAdmin);



//4 done
const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}

const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullProfile);


//5 
const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

const booksSecond = [...books, bookToAdd];
console.log(booksSecond);

//6 
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const employee2 = {...employee, age: 25, salary: 3000};

console.log(employee2);

//7 
const array = ['value', () => 'showValue'];
const [value, showValue] = array;

console.log(value); // має бути виведено 'value'
console.log(showValue());  // має бути виведено 'showValue'






























// console.log("Request data...");
// const p = new Promise(function (resolve, reject) {
//   setTimeout(() => {
//     console.log("Preparing data...")
//     const backendData = {
//       server: "aws",
//       port: 2000,
//       status: "working"
//     }
//     resolve(backendData)
//   }, 2000)
// })

// p.then(data => {
//     return new Promise((resolve, reject) => {
//       setTimeout(() => {
//         data.modified = true
//         resolve(data)
//       }, 2000)
//     })

//   })
//   .then(clientData => {
//     console.log("Data received", clientData)
//     clientData.fromPromise = true;
//     return clientData
//   }).then(data => {
//     console.log("Modified", data)
//   })
//   .catch((err) => console.log("Error: " + err))
//   .finally(() => console.log("Finally"))
